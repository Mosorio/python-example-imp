import numpy as np
from cv2 import cv2
#https://living-sun.com/es/python/727988-how-to-write-text-on-a-image-in-windows-using-python-opencv2-python-windows-opencv.html

# Load an color image in grayscale 
img = cv2.imread('C:/Users/Mijail QA/Documents/ToolAutomateCliente/toolautometricaspy/Valeria-Proyect/test.jpg',1)

# Write some Text

font                   = cv2.FONT_HERSHEY_SIMPLEX
bottomLeftCornerOfText = (300,500)
fontScale              = 0.5
fontColor              = (255,255,255)
lineType               = 1

cv2.putText(img,"Hello World!",
bottomLeftCornerOfText,
font,
fontScale,
fontColor,
lineType)

#Display the image
cv2.imshow("img",img)

#Save image
cv2.imwrite("out.jpg", img)

cv2.waitKey(0)