import json
import os, sys
from io import open
import smtplib
# import pymysql.cursors
import datetime 
import time
import uuid 

#///////////////////////////////////////////
# Funcion para obtener todas las lineas de Identificacion, desde el archivo de Log
#//////////////////////////////////////////
def GetVersionClient():
    #//////////////////////////////////
	# Verificar la cantidad de archivos que existen dentro del directorio
	# Indicamos la ruta de donde se quieren leer los archivos
    path= "C:/ProgramData/Vsblty/KingSalmon/"
    dirs = os.listdir(path)
    i = 1
    # ////////////////////////////////////////////////////////////
    # Por cada Archivo dentro del directorio, se realiza un ciclo for para recorrer cada linea y verificar hits  
    # ////////////////////////////////////////////////////////////
    for file in dirs:
        ruta = "C:/ProgramData/Vsblty/KingSalmon/"
        archivo = file
        rutacompleta = ruta + archivo
        print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        print ("File: ",rutacompleta)
        print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
        # La aplicaicon lee y guarada en la variable "archivo_texto" toda la informacion 
        archivo_texto=open (rutacompleta, "r")

        # Se lee Linea por linea
        lineas_texto=archivo_texto.readlines()

        # ////////////////////////////////////////////////////////////
        # Inicio de Busqueda
        # Por cada linea se valida si contiene el Texto que se desea encontrar 
        # Para este caso es: "[EDGE Detection] Identified Person >> Name:"
        # ////////////////////////////////////////////////////////////

        for ClientLine in lineas_texto:
            if "[EDGE Detection] Identified Person >> Name:" in ClientLine: #busqueda por el parametro 
                # ////////////////////////////////////////////////////////////
                # Obtener la posicion de cada Propiedad
                # ////////////////////////////////////////////////////////////
                Timeline = ClientLine[0:19]
                NamePerson = ClientLine[88:-220]
                PosicionPersonId = ClientLine.find("PersonId") + 10
                PosicionGroupId  = ClientLine.find("GroupId") + 9
                PosicionMatchProbability = ClientLine.find("MatchProbability") + 18
                PosicionLocalPersistedFaceId = ClientLine.find("LocalPersistedFaceId") + 22
                # ////////////////////////////////////////////////////////////
                # Obtener el valor de cada propiedad segun la posicion
                # ///////////////////////////////////////////////////////////
                PersonId = ClientLine[PosicionPersonId:-172]
                GroupId = ClientLine[PosicionGroupId:-125]
                MatchProbability = ClientLine[PosicionMatchProbability:-100]
                LocalPersistedFaceId = ClientLine[PosicionLocalPersistedFaceId:-39]
                # ////////////////////////////////////////////////////////////
                # imprimir los valores encontrados en cada linea
                # ///////////////////////////////////////////////////////////
                print ("Item:              ", i)
                print ("File:              ", archivo)
                print ("Time:              ", Timeline)
                print ("Name:              ", NamePerson)
                print ("PersonId:          ", PersonId)
                print ("Match Probability: ", MatchProbability, "%")
                print ("GroupId:           ", GroupId)
                print ("LocalPersistedId:  ", LocalPersistedFaceId)
                print ("/////////////////////////////////////////////////////////////////////////////////////////////////////////////")
                # input ()
                i +=1

    return 

GetVersionClient()
input ()
